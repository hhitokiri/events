import React, {useState, useEffect} from 'react';
import Question from './components/Question';
import Expense_form from './components/expense_form';
import ListSpending from './components/ListSpending';
import BudgetControl from './components/BudgetControl';

function App() {
  const [budget, saveBudget] = useState(0);
  const [remaining, saveRemaining] = useState(0);
  const [seebudget, saveseebudget] = useState(true)
  const [createseebudget, savecreateseebudget] = useState(false)
  const [spending, saveSpending] = useState({})
  const [spendings, saveSpendings] = useState([])
  // saveSpendings(spending)
  // console.log(spendings)

  useEffect(() =>{
    if(createseebudget){
      const listSpending = [...spendings, spending]
      saveSpendings(listSpending)
      const operation = remaining - spending.spendingAmount
      saveRemaining(operation)
      savecreateseebudget(false)
    }

  }, [createseebudget, spendings,spending, remaining ])
  return (
    <div className="App container">
      <header >
          <h1> weekly expenses </h1>
          <div className="contenido-principal contenido">
            {(seebudget) ? 
                  <Question
                  saveBudget={saveBudget}
                  saveseebudget={saveseebudget}
                  saveRemaining={saveRemaining}
                  />              
              :(
                <div className="row">
                  <div className="one-half column">
                    <Expense_form
                    saveSpending={saveSpending}
                    savecreateseebudget={savecreateseebudget}
                    />
                  </div>
                  <div className="one-half column"> 
                  <ListSpending
                  list={spendings}
                  />
                  <BudgetControl 
                    budget={budget}
                    remaining={remaining}
                  />
                  </div>
                </div>
              )
              
              }
              
          </div>
          
      </header>
    </div>
  );
}

export default App;
