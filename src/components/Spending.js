import React from 'react'

const Spending = ({spending}) => {
    return (
        <li className="gastos" >
            <p>
                {spending.spendingName}
                <span className="gasto" >$ {spending.spendingAmount}</span>
            </p>
        </li>
    )
}

export default Spending
