import React , {Fragment} from 'react'
import { ReviewBudget } from './Helpers';

const BudgetControl = ({budget, remaining}) => {
    return (
       <Fragment>
           <div className="alert alert-primary">
                Budget:$ {budget}
           </div>
           <div className={ReviewBudget(budget, remaining)}>
                Remaining:$ {remaining}
           </div>
       </Fragment>
    )
}

export default BudgetControl
