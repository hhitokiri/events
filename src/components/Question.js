import React, {Fragment, useState} from 'react'
import Error from './Error';

export default function Question(props) {
    const {saveBudget, saveseebudget, saveRemaining} = props;
    const [quantity, saveQuantity] = useState(0)
    const [erro, saveError] = useState(false)
    const addBudge = e =>{
        e.preventDefault();
        if(quantity <= 0 || isNaN(quantity)){
            saveError(true);
            return;
        }
        saveError(false);
        saveBudget(quantity)
        saveRemaining(quantity)
        saveseebudget(false)
    }
    return (
        <Fragment>
            <h2>what is your budget</h2>
            <form onSubmit={addBudge}>
                {erro ? 
                    <Error
                    messaje = "the expense can not be empty or less than zero"
                    />
                    :null}
                <input className="u-full-width" placeholder="put your budget" type="number" name="budget" id="budget"
                onChange={e =>(saveQuantity(parseInt(e.target.value)))}
                />
                <input type="submit" className="button-primary u-full-width" value="Define budget"/>
            </form>
        </Fragment>
    )
}
