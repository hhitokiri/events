import React from 'react'
import Spending from './Spending';

const ListSpending = (props) => {
    const {list} = props
    return (
        <div className="gastos-realizados">
            <h2>List</h2>
            {list.map(spending =>(
                <Spending 
                    key={spending.id}
                    spending= {spending}
                />
            ))}
            
        </div>
    )
}

export default ListSpending
