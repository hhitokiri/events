import React, {useState} from 'react'
import uuid from 'uuid'
import Error from './Error';

export default function Expense_form(props) {
    const [spendingName, saveSpendingName] = useState('')
    const [spendingAmount, saveSpendingAmount] = useState(0)
    const [erro, saveError] = useState(false)
    const {saveSpending, savecreateseebudget} = props
    const addSpending = e =>{
        e.preventDefault()

        if(spendingAmount <= 0 || isNaN(spendingAmount) || spendingName === ''){
            saveError(true);
            return;
        }
        savecreateseebudget(true)
        const spending = {
            spendingName,
            spendingAmount,
            id:uuid()
        }
        console.log(uuid())

        saveSpending(spending);
        saveError(false);
        saveSpendingName("")
        saveSpendingAmount(0)

    }
    return (
        <form onSubmit={addSpending}>
            
            <h2>add your expenses</h2>
            {erro ? 
                    <Error
                    messaje = "the expense can not be empty or less than zero"
                    />
                    :null}
            <label htmlFor="spending_name">spending name</label>
            <input id="spending_name" className="u-full-width" type="text"
            onChange={e => (saveSpendingName(e.target.value))}
            value={spendingName}
            />
            <label htmlFor="spending_amount">spending amount</label>
            <input id="spending_amount" className="u-full-width" type="number"
            onChange={e => (saveSpendingAmount(parseInt(e.target.value)))}
            value={spendingAmount}
            />
            <input type="submit" className="button-primary u-full-width" value="add expense"/>
        </form>
    )
}
